import React from "react";

import { Conference as ConferenceEntity } from "../../entities/Conference";
import { formatDates } from "../../util/date";

import "./style.css";

export function Conference({
  name,
  date,
  location,
  cocUrl,
  hasCfp,
  hasCoc,
  cfpUrl,
  url,
  isConcluded
}: ConferenceEntity) {
  return (
    <div className={`Conference ${isConcluded ? "Conference--isConcluded" : ""}`}>
      <h1 className="Conference__Title">
        <a className="Conference__Link" href={url} target="blank">
          {name}
        </a>
      </h1>

      <h2 className="Conference__Schedule">{formatDates(date.start, date.end)}</h2>

      <p className="Conference__Location">
        {location.city}, {location.country}
      </p>

      <footer className="Conference__Footer">
        {hasCoc && (
          <a className="Conference__Footer__Item Conference__CoC" href={cocUrl}>
            CoC
          </a>
        )}
        {hasCfp && (
          <a className="Conference__Footer__Item Conference__C4P" href={cfpUrl}>
            C4P
          </a>
        )}
      </footer>
    </div>
  );
}
