import React from "react";

import "./style.css";

function Header({ children }: { children: React.ReactNode }) {
  return <header className="Layout__Header">{children}</header>;
}

function Main({ children }: { children: React.ReactNode }) {
  return <main className="Layout__Main">{children}</main>;
}

export class Layout extends React.Component {
  public static Header = Header;
  public static Main = Main;
}
