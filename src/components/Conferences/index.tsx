import React from "react";

import { Conference as ConferenceEntity } from "../../entities/Conference";
import { Conference } from "../Conference";

import "./style.css";

export function Conferences({ conferences }: { conferences: ConferenceEntity[] }) {
  const conferencesWithCoc = conferences.filter(conf => conf.hasCoc);
  const conferencesWithoutCoc = conferences.filter(conf => !conf.hasCoc);

  return (
    <div className="Conferences">
      {conferencesWithCoc.map((conference, idx) => (
        <Conference key={idx} {...conference} />
      ))}

      {conferencesWithoutCoc.map((conference, idx) => (
        <Conference key={idx} {...conference} />
      ))}
    </div>
  );
}
